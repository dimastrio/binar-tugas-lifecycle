package id.dimas.activitylifecycle

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Toast.makeText(this, "OnCreate", Toast.LENGTH_SHORT).show()
        Log.e("Activity Lifecycle","OnCreate")
    }

    override fun onStart() {
        super.onStart()
        Toast.makeText(this, "OnStart", Toast.LENGTH_SHORT).show()
        Log.e("Activity Lifecycle","OnStart")
    }

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "OnResume", Toast.LENGTH_SHORT).show()
        Log.e("Activity Lifecycle","OnResume")
    }

    override fun onPause() {
        super.onPause()
        Toast.makeText(this, "OnPause", Toast.LENGTH_SHORT).show()
        Log.e("Activity Lifecycle","OnPause")
    }

    override fun onStop() {
        super.onStop()
        Toast.makeText(this, "OnStop", Toast.LENGTH_SHORT).show()
        Log.e("Activity Lifecycle","OnStop")
    }

    override fun onRestart() {
        super.onRestart()
        Toast.makeText(this, "OnRestart", Toast.LENGTH_SHORT).show()
        Log.e("Activity Lifecycle","OnRestart")
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(this, "OnDestroy", Toast.LENGTH_SHORT).show()
        Log.e("Activity Lifecycle","OnDestroy")
    }
}

/** Potrait to Landscape
 *  - OnPause
 *  - OnStop
 *  - OnDestroy
 *  - OnCreate
 *  - OnStart
 *  - OnResume
 */

/** Landscape to Potrait
 *  - OnPause
 *  - OnStop
 *  - OnDestroy
 *  - OnCreate
 *  - OnStart
 *  - OnResume
 */